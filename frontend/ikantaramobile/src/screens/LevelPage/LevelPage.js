/**
 * Created by mata on 12/12/18.
 */

/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    ImageBackground
} from "react-native";
import { connect } from "react-redux";
import backgroundImage from "../../assets/level_bg.png";
import {Navigation} from "react-native-navigation";

class LevelPage extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };


    OnSelect = () => {
        console.log('start play')
        Navigation.startSingleScreenApp({
            screen: {
                screen: "ikantara.ChooseFish",
                title: "IKANTARA"
            },

        });
    };

    render() {

        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <View>
                        <TouchableOpacity onPress={this.OnStartButton}>
                            <View style={styles.lvlbtn}>
                                <ImageBackground style={styles.imglvlbtn}
                                                 source={require('../../assets/lvl_btn.png')}>
                                    <View style={styles.lvlword}>
                                        <Text style={styles.textlvl}>Level 1</Text>
                                    </View>

                                </ImageBackground>


                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.OnStartButton}>
                            <View style={styles.lvlbtn}>
                                <Image style={styles.imglvlbtn}
                                       source={require('../../assets/lvl_btn.png')}
                                />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.OnStartButton}>
                            <View style={styles.lvlbtn}>
                                <Image style={styles.imglvlbtn}
                                       source={require('../../assets/lockedlvl_btn.png')}
                                />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.OnStartButton}>
                            <View style={styles.lvlbtn}>
                                <Image style={styles.imglvlbtn}
                                       source={require('../../assets/lockedlvl_btn.png')}
                                />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.OnStartButton}>
                            <View style={styles.lvlbtn}>
                                <Image style={styles.imglvlbtn}
                                       source={require('../../assets/lockedlvl_btn.png')}
                                />
                            </View>
                        </TouchableOpacity>

                    </View>

                </View>

                <View style={{flex:2, alignItems:'center'}}>
                        <View style={{marginTop: 75, marginLeft: 50, marginBottom: 30}}>
                            <Image style={{height: 200, width: 400}}
                                source={require('../../assets/sumatera_hijau.png')}
                            />
                        </View>
                    <TouchableOpacity onPress={this.OnSelect}>
                        <View style={styles.lvlbtn}>
                            <Image style={styles.imglvlbtn}
                                   source={require('../../assets/select_btn.png')}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,
        flexDirection: "row"
    },
    imglvlbtn: {
        height: 50,
        width: 200,
        alignSelf: 'stretch'
    },
    lvlbtn: {
        paddingVertical: 5,
    },
    lvlword: {
        flex: 1,
        justifyContent:'center', alignItems:'center'},
    textlvl: {
        color: "#ffffff",
        fontSize: 20,
        fontWeight: "bold"
    }
})

export default connect(null, null)(LevelPage);