
import React,{ Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    PanResponder,
    Animated,
    Easing,
    Dimensions, Image, ImageBackground, TouchableOpacity
} from 'react-native';
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";

import backgroundImage from "../../assets/game_bg.png";

class GamePage extends Component{
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };

    constructor(props) {
        super(props);
        this.top1=0;
        this.top2=0;
        this.top3=0;
        this.top4=0;
        this.left1=0;
        this.left2=0;
        this.left3=0;
        this.left4=0;
        this.view1=null;
        this.view2=null;
        this.view3=null;
        this.view4=null;
        this.customStyle1={
            style:{
                top:this.top1,
                left:this.left1,
            }
        };
        this.customStyle2={
            style:{
                top:this.top,
                left:this.left,
            }
        };
        this.customStyle3={
            style:{
                top:this.top,
                left:this.left,
            }
        };
        this.customStyle4={
            style:{
                top:this.top,
                left:this.left,
            }
        };

    }


    componentWillMount() {
        this.panResponder1=PanResponder.create({
            onStartShouldSetPanResponder: (event, gestureState) => true,
            onMoveShouldSetPanResponder: (event, gestureState) => true,
            onPanResponderMove: this._onPanResponderMove1.bind(this),
            onPanResponderRelease: this._onPanResponderRelease1.bind(this),
        });
        this.panResponder2=PanResponder.create({
            onStartShouldSetPanResponder: (event, gestureState) => true,
            onMoveShouldSetPanResponder: (event, gestureState) => true,
            onPanResponderMove: this._onPanResponderMove2.bind(this),
            onPanResponderRelease: this._onPanResponderRelease2.bind(this),
        });
        this.panResponder3=PanResponder.create({
            onStartShouldSetPanResponder: (event, gestureState) => true,
            onMoveShouldSetPanResponder: (event, gestureState) => true,
            onPanResponderMove: this._onPanResponderMove3.bind(this),
            onPanResponderRelease: this._onPanResponderRelease3.bind(this),
        });
        this.panResponder4=PanResponder.create({
            onStartShouldSetPanResponder: (event, gestureState) => true,
            onMoveShouldSetPanResponder: (event, gestureState) => true,
            onPanResponderMove: this._onPanResponderMove4.bind(this),
            onPanResponderRelease: this._onPanResponderRelease4.bind(this),
        });
    }
    _onPanResponderRelease1(event, gestureState){
        this.top1 += gestureState.dy;
        this.left1 += gestureState.dx;
        // this.top1 += gestureState.dy;
        // this.left1 += gestureState.dx;
    }

    updateNativeProps1(){
        this.view1 && this.view1.setNativeProps(this.customStyle1);
    }

    _onPanResponderMove1(event, gestureState){
        //alert(event);
        this.customStyle1.style.top=this.top1+gestureState.dy;
        this.customStyle1.style.left=this.left1+gestureState.dx;
        this.updateNativeProps1()
    }

    _onPanResponderRelease2(event, gestureState){
        this.top2 += gestureState.dy;
        this.left2 += gestureState.dx;
    }


    updateNativeProps2(){
        this.view2 && this.view2.setNativeProps(this.customStyle2);
    }

    _onPanResponderMove2(event, gestureState){
        //alert(event);
        this.customStyle2.style.top=this.top2+gestureState.dy;
        this.customStyle2.style.left=this.left2+gestureState.dx;
        this.updateNativeProps2()
    }

    _onPanResponderRelease3(event, gestureState){
        this.top3 += gestureState.dy;
        this.left3 += gestureState.dx;
    }


    updateNativeProps3(){
        this.view3 && this.view3.setNativeProps(this.customStyle3);
    }

    _onPanResponderMove3(event, gestureState){
        //alert(event);
        this.customStyle3.style.top=this.top3+gestureState.dy;
        this.customStyle3.style.left=this.left3+gestureState.dx;
        this.updateNativeProps3()
    }

    _onPanResponderRelease4(event, gestureState){
        this.top4 += gestureState.dy;
        this.left4 += gestureState.dx;
    }


    updateNativeProps4(){
        this.view4 && this.view4.setNativeProps(this.customStyle4);
    }

    _onPanResponderMove4(event, gestureState){
        //alert(event);
        this.customStyle4.style.top=this.top4+gestureState.dy;
        this.customStyle4.style.left=this.left4+gestureState.dx;
        this.updateNativeProps4()
    }

    OnSelect = () => {
        console.log('start play')
        Navigation.startSingleScreenApp({
            screen: {
                screen: "ikantara.WinPage",
                title: "IKANTARA"
            },

        });
    };


    render(){
        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View>
                    <View style={styles.bigContainer}>
                        <View style={styles.container}>
                            <View ref={view => this.view1 = view}
                                  {...this.panResponder1.panHandlers}>
                                <Image
                                    source={require('../../assets/gambar_penyu1.png')}
                                />
                            </View>
                            <View ref={view => this.view2 = view}
                                  {...this.panResponder2.panHandlers}>
                                <Image
                                    source={require('../../assets/gambar_penyu2.png')}
                                />
                            </View>
                        </View>
                        <View style={styles.container}>
                            <View ref={view => this.view3 = view}
                                  {...this.panResponder3.panHandlers}>
                                <Image
                                    source={require('../../assets/gambar_penyu3.png')}
                                />
                            </View>
                            <View ref={view => this.view4 = view}
                                  {...this.panResponder4.panHandlers}>
                                <Image
                                    source={require('../../assets/gambar_penyu4.png')}
                                />
                            </View>
                        </View>
                    </View>
                </View>
                <TouchableOpacity onPress={this.OnSelect}>
                    <View>
                        <Image
                            source={require('../../assets/play_home_button.png')}
                        />
                    </View>
                </TouchableOpacity>
            </ImageBackground>

        );
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,
    },
    bigContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    container: {
        flex: 1,
        flexDirection: 'row'
    },
    dropZone    : {
        height  : 100,
        width: 100,
        backgroundColor:'#2c3e50'
    },
});


export default connect(null, null)(GamePage);
