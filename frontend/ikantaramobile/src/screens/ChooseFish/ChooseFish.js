/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";

import backgroundImage from "../../assets/ikan_bg.png";

class ChooseFish extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };


    OnClickIkan1 = () => {
        console.log('start play')
        Navigation.startSingleScreenApp({
            screen: {
                screen: "ikantara.GamePage",
                title: "IKANTARA"
            },

        });
    };

    OnClickIkan2 = () => {
        console.log('start play')
        Navigation.startSingleScreenApp({
            screen: {
                screen: "ikantara.GameManip",
                title: "IKANTARA"
            },

        });
    };

    OnClickIkan3 = () => {
        console.log('start play')
        Navigation.startSingleScreenApp({
            screen: {
                screen: "ikantara.GamePage",
                title: "IKANTARA"
            },

        });
    };


    render() {

        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={{flex:1, flexDirection: "row", justifyContent:'center', alignItems:'center'}}>
                    <TouchableOpacity onPress={this.OnClickIkan1}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan1.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnClickIkan2}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan2.png')}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.OnClickIkan3}>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan3.png')}
                            />
                        </View>
                    </TouchableOpacity>
                        <View style={styles.viewFish}>
                            <Image style={styles.imgFish}
                                   source={require('../../assets/ikan4.png')}
                            />
                        </View>
                </View>

                <View style={{flex:1, flexDirection: "row", justifyContent:'center', alignItems:'center'}}>
                    <View style={styles.viewFish}>
                        <Image style={styles.imgFish}
                               source={require('../../assets/ikan5.png')}
                        />
                    </View>
                    <View style={styles.viewFish}>
                        <Image style={styles.imgFish}
                               source={require('../../assets/ikan6.png')}
                        />
                    </View>
                    <View style={styles.viewFish}>
                        <Image style={styles.imgFish}
                               source={require('../../assets/ikan7.png')}
                        />
                    </View>
                    <View style={styles.viewFish}>
                        <Image style={styles.imgFish}
                               source={require('../../assets/ikan8.png')}
                        />
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,

    },
    imgFish: {
        width: 100,
        height: 100,
    },
    viewFish: {
        padding: 10,
    }
})

export default connect(null, null)(ChooseFish);