/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    FlatList,
    ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";

import backgroundImage from "../../assets/game_bg.png";

class WinPage extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: 'white',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };


    OnStartButton = () => {
        console.log('start play')
        Navigation.startSingleScreenApp({
            screen: {
                screen: "ikantara.LevelPage",
                title: "IKANTARA"
            },

        });
    };


    render() {

        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <Image
                        source={require('../../assets/ikantara_texthome.png')}
                    />
                </View>
                <View style={{flex:1, flexDirection: "row", justifyContent:'center', alignItems:'center'}}>
                    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                        <TouchableOpacity onPress={this.OnStartButton}>
                            <View>
                                <Image
                                    source={require('../../assets/play_home_button.png')}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>


            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1,
    },
    imgbtn: {
        alignItems: "stretch",
        width: 10,
    },
})

export default connect(null, null)(WinPage);