import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import React, { Component } from 'react';

import MainMenu from "./src/screens/MainMenu/MainMenu";
import LevelPage from "./src/screens/LevelPage/LevelPage";
import ChooseFish from "./src/screens/ChooseFish/ChooseFish";
import GamePage from "./src/screens/GamePage/GamePage";
import configureStore from "./src/store/configureStore";
import WinPage from "./src/screens/WinPage/WinPage";
const store = configureStore();

// Register Screens
Navigation.registerComponent(
    "ikantara.MainMenu",
    () => MainMenu,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.LevelPage",
    () => LevelPage,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.ChooseFish",
    () => ChooseFish,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.GamePage",
    () => GamePage,
    store,
    Provider
);

Navigation.registerComponent(
    "ikantara.WinPage",
    () => WinPage,
    store,
    Provider
);

Navigation.startSingleScreenApp({
    screen: {
        screen: "ikantara.MainMenu",
        title: "IKANTARA"
    },

});